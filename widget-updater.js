class WidgetUpdater {
    constructor({widgets = []}) {
        this.widgets = widgets;
    }

    getUpdatedWidgets(forced) {
        let updateTime = Date.now();
        let updated = [];

        this.widgets.forEach((widget) => {
            if (forced || widget.shouldUpdate(updateTime)) {
                let data = widget.updateData(updateTime);
                if (data) {
                    updated.push({
                        id: widget.getId(),
                        type: widget.getType(),
                        data: data
                    });
                }
            }
        });
        return updated;
    }
}

module.exports = WidgetUpdater;