class Widget {
    constructor({id, updateInterval}) {
        this.id = id;
        this.lastUpdateTime = 0;
        this.updateInterval = updateInterval;
    }

    shouldUpdate(updateTime) {
        if (this.updateInterval) {
            return updateTime - this.lastUpdateTime > this.updateInterval;
        } else {
            return this.lastUpdateTime === 0;
        }
    }

    updateData(updateTime) {
        this.lastUpdateTime = updateTime;
    }

    getId() {
        return this.id;
    }

    getType() {
        return null;
    }

}

module.exports = Widget;