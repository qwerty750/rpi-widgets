let Widget = require('./Widget');

function getRandomizer(bottom, top) {
    return Math.floor( Math.random() * ( 1 + top - bottom ) ) + bottom;
}

class ImageWidget extends Widget {
    constructor(params) {
        super(params);
    }

    getType() {
        return 'image';
    }

    updateData(updateTime) {
        super.updateData(updateTime);
        return {
            src: `http://www.ux.uis.no/~tranden/brodatz/D${getRandomizer(0, 100)}.gif`
        }
    }
}

module.exports = ImageWidget;