let Widget = require('./Widget');
var exec = require('child_process').exec;

class StdoutWidget extends Widget {

    constructor(params) {
        super(params);
        this.text = "";
    }

    getType() {
        return 'stdout';
    }

    updateData(updateTime) {
        super.updateData(updateTime);

        exec(__dirname + "/echo.sh", (err, stdout, stderr) => {
            this.text = stdout;
        });

        let text = this.text;
        this.text = "";
        if (text !== "") {
            return {
                text: text
            }
        }

        return false;
    }
}

module.exports = StdoutWidget;