let Widget = require('./Widget');
const request = require('request');

class ChuckJokeWidget extends Widget {

    constructor(params) {
        super(params);
        this.text = "";
    }

    getType() {
        return 'chuckJoke';
    }

    updateData(updateTime) {
        super.updateData(updateTime);

        request('https://api.chucknorris.io/jokes/random', { json: true }, (err, res, body) => {
            if (err) { return console.log(err); }
            this.text = body.value;
        });

        let text = this.text;
        this.text = "";
        if (text !== "") {
            return {
                text: text
            }
        }

        return false;
    }
}

module.exports = ChuckJokeWidget;