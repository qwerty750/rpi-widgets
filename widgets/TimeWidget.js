let Widget = require('./Widget');
class TimeWidget extends Widget {
    constructor(params) {
        super(params);
    }

    updateData(updateTime) {
        super.updateData(updateTime);
        return {
            time: Date.now()
        }
    }

    getType() {
        return "time";
    }
}

module.exports = TimeWidget;