var express = require('express');

var app = express();
var wss = require('express-ws')(app);

app.ws('/', function(ws, req) {
});

var aWss = wss.getWss('/');

let WidgetUpdater = require('./widget-updater');
let TimeWidget = require('./widgets/TimeWidget');
let ImageWidget = require('./widgets/ImageWIdget');
let ChuckJokeWidget = require('./widgets/ChuckJokeWidget');
let StdoutWidget = require('./widgets/StdoutWidget');

let widgetUpdater = new WidgetUpdater({
    widgets: [
        new TimeWidget({
            id: "time",
            updateInterval: 200
        }),
        new TimeWidget({
            id: "time2"
        }),
        new ImageWidget({
            id: "image1",
            updateInterval: 200
        }),
        new ImageWidget({
            id: "image2",
            updateInterval: 200
        }),
        new ImageWidget({
            id: "image3",
            updateInterval: 200
        }),
        new ImageWidget({
            id: "image4",
            updateInterval: 200
        }),
        new ChuckJokeWidget({
            id: "chuckJoke",
            updateInterval: 2000
        }),
        new StdoutWidget({
            id: "stdout",
            updateInterval: 50
        })
    ],
});

setInterval(function () {
    if (aWss.clients) {
        let widgets = widgetUpdater.getUpdatedWidgets();
        if (widgets.length > 0) {
            aWss.clients.forEach(function (client) {
                client.send(JSON.stringify(widgets));
            });
        }
    }
}, 50);

aWss.on('connection', function(ws) {
    let widgets = widgetUpdater.getUpdatedWidgets(true);
    if (widgets.length > 0) {
        ws.send(JSON.stringify(widgets));
    }
});

module.exports = app;